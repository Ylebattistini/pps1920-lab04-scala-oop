package u04lab.code

import Lists._
import Optionals._
import u04lab.code.Streams.Stream.Cons

import scala.util.Random

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]):PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

  import Streams._

 class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

   override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = new FromStream[Int](Stream.iterate(start)(successive))

   override def fromList[A](list: List[A]): PowerIterator[A] = new FromStream[A](List.toStream(list))

   override def randomBooleans(size: Int): PowerIterator[Boolean] = new FromStream[Boolean](Stream.take(Stream.generate(Random.nextBoolean))(size))
 }

class FromStream[A](var s :Stream[A]) extends PowerIterator[A]{
  import Optionals._

  var parList:List[A]= List.Nil()

  override def next(): Option[A] = s match{
    case Cons(head, tail) => s=tail()
      parList = List.Cons(head(), parList)
      Optionals.Option.of(head())
    case _=> Optionals.Option.empty
  }
  override def allSoFar(): List[A] = List.reverse(parList)

  override def reversed(): PowerIterator[A] = new FromStream[A](List.toStream(allSoFar()))
}

